# Findcoms #

Find virtual serial (COM) port names for connected Arduinos and Pololu
AVR programmers. Write a file with the discovered name so that you can
use it in a makefile.

## Prerequisites ##

Findcoms is written in Tcl, and thus needs Tcl to
run. [ActiveState](https://www.activestate.com/products/tcl/) has nice
installers for Windows and Linux. The ActiveTcl distribution comes
with the
[textutil](http://docs.activestate.com/activetcl/8.5/tcl/tcllib/textutil/textutil.html)
package, part of Tcllib, which you'll also need.

If you're using Windows, you'll need the
[TWAPI](https://twapi.magicsplat.com/) package. The ActiveState
distribution includes this, as does [Magicsplat Tcl/Tk for
Windows](https://www.magicsplat.com/tcl-installer/index.html).

## What devices does it detect? ##

Starting Findcoms with the `-d?` flag will tell you which devices it
can detect.  The screenshot below shows this along with the `TCLLIB`
environment variable you'll need.

![List d params shot](/images/list_d_params_shot.png)

## Detecting a device (Windows) ##

The screenshot below shows Findcoms looking for a connected [Pololu USB AVR
Programmer v2](https://www.pololu.com/product/3170).  This is a
composite USB device -- it shows up as two COM ports.  One is a
USB/UART interface, and the other is an AVR ISP programmer.  Findcoms
writes the COM port for the ISP programmer to `vcp.txt` -- the default filename.

![Pololu isp detection shot](/images/pololu_isp_detection_shot.png)

## Using the detected device in a makefile ##

You can use the contents of `vcp.txt` in a makefile with a shell
command:

isp_vcp = $(shell cat vcp.txt)

You can add `vcp.txt` as a prerequisite for something like a
`burn_flash` target to make sure the file is updated before the recipe
is executed.



