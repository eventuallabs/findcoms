######################## Global configuration ########################

# The name of this program.  This will get used to identify logfiles,
# configuration files and other file outputs.
set program_name findcoms

set thisfile [file normalize [info script]]

# Directory where this script lives
set program_directory [file dirname $thisfile]

# Directory from which the script was invoked
set invoked_directory [pwd]

# This software's version.  Anything set here will be clobbered by the
# makefile when starpacks are built.
set revcode 1.0

set root_directory [file dirname $argv0]

######################## Command line parsing ########################

package require cmdline
set usage "usage: [file tail $argv0] \[options]"

set options {
    {a  "List all discovered devices"}
    {d.arg "pololu_isp" "Device name to find (see list with d?)"}
    {d? "List available device names"}
    {o.arg "vcp.txt" "Output file name"}

}

try {
    array set params [::cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {message optdict} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $message
    exit 1
}

if {$tcl_platform(os) eq "Windows NT"} {
    # To access device information from the Windows registry
    package require twapi
}

# Provides splitx needed to split strings on substrings
package require textutil

proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterint 0 $width] {
	append dashline "-"
    }
    return $dashline
}

proc get_windows_hwid_value {hwid delimiter} {
    # Return a value from a windows hardware ID
    #
    # Arguments:
    #   hwid -- Windows hardware ID, like USB\VID_1FFB&PID_00BB&REV_0102&MI_01
    #   delimiter -- Substring like "VID_" or "PID_"

    # Prefix string will start with the value we want terminated in a &
    set prefix_string [lindex [textutil::splitx $hwid $delimiter] 1]
    set value [string trimleft [lindex [split $prefix_string &] 0] 0]
    return $value
}

proc detect_vcps {} {
    # Windows -- returns a dictionary of:
    #
    # (COM port): vendor_id, product_id, composite_index
    set vcp_dict [dict create]
    switch $::tcl_platform(os) {
	{Linux} {
	    set nodelist ""
	    try {
		# FTDI devices show up as ttyUSB*
		set nodelist [glob -nocomplain -directory /dev/ ttyUSB*]
		# Arduino devices show up as ttyACM*
		lappend nodelist [glob -nocomplain -directory /dev/ ttyACM*]
	    } trap {} {message optdict} {
		puts $message
		set nodelist ""
	    }
	    foreach vcp [join $nodelist] {
		try {
		    set udev_data [exec udevadm info --query=property --name $vcp]
		    set id_serial ""
		    set id_usb_interface_number ""
		    foreach line [split $udev_data "\n"] {
			if {[string first "ID_SERIAL=" $line] >= 0} {
			    set id_serial [lindex [split $line "= \n"] 1]
			}
			if {[string first "ID_USB_INTERFACE_NUM=" $line] >= 0} {
			    set composite_index [string trimleft [lindex [split $line "= \n"] 1] 0]
			}
			if {[string first "ID_VENDOR_ID=" $line] >= 0} {
			    set vendor_id [string trimleft [lindex [split $line "= \n"] 1] 0]
			}
			if {[string first "ID_MODEL_ID=" $line] >= 0} {
			    set product_id [string trimleft [lindex [split $line "= \n"] 1] 0]
			}

		    }
		} trap CHILDSTATUS {results options} {
		    puts "oops -- $results"
		    set status [lindex [dict get $options -errorcode] 2]
		    continue
		}
		puts "ID_SERIAL is $id_serial"
		puts "  Vendor ID: $vendor_id"
		puts "  Product ID: $product_id"
		puts "  COM port: $vcp"
		puts "  Composite index: $composite_index"
		dict set vcp_dict $vcp vendor_id $vendor_id
		dict set vcp_dict $vcp product_id $product_id
		dict set vcp_dict $vcp composite_index $composite_index
	    }
	}
	{Windows NT} {
	    # Grab the device info set for present objects only
	    set dfs [twapi::devinfoset -presentonly true]
	    foreach element [twapi::devinfoset_elements $dfs] {
		catch {twapi::devinfoset_element_registry_property $dfs $element class} classname
		if {$classname eq "Ports"} {
		    # USB/serial ports are part of the "ports" class
		    catch {twapi::devinfoset_element_registry_property $dfs $element friendlyname} friendlyname
		    puts "Friendlyname is $friendlyname"
		    # Get the COMn number from the friendlyname.  Friendlynames look like
		    #
		    # USB Serial Device (COM5)
		    set vcp [lindex [split $friendlyname "()"] 1]
		    catch {twapi::devinfoset_element_registry_property $dfs $element mfg} mfg
		    puts "  Manufacturer is $mfg"
		    catch {twapi::devinfoset_element_registry_property $dfs $element hardwareid} hardwareid_list
		    # There may be more than one entry in the hardare ID list.  Just use the first one.
		    set hwid [lindex $hardwareid_list 0]
		    puts "  Hardwareid is $hwid"
		    set vendor_id [get_windows_hwid_value $hwid "VID_"]
		    set product_id [get_windows_hwid_value $hwid "PID_"]
		    set composite_index [get_windows_hwid_value $hwid "MI_"]
		    puts "  Vendor ID: $vendor_id"
		    puts "  Product ID: $product_id"
		    puts "  COM port: $vcp"
		    puts "  Composite index: $composite_index"
		    dict set vcp_dict $vcp vendor_id $vendor_id
		    dict set vcp_dict $vcp product_id $product_id
		    dict set vcp_dict $vcp composite_index $composite_index
		}
	    }
	    twapi::devinfoset_close $dfs
	}
	default {
	    # This should never happen
	    puts "Failed to detect OS"
	}
    }

    return $vcp_dict
}

proc get_pololu_isp_vcp {} {
    # Return the Virtual Com Port corresponding to the Pololu programmer
    #
    # The Pololu vendor ID is 1FFB
    set vid_match_list [list 1FFB]
    set vcp_dict [detect_vcps]
    foreach {vcp} [dict keys $vcp_dict] {
	puts "Found $vcp"
	set vid [dict get $vcp_dict $vcp vendor_id]
	set pid [dict get $vcp_dict $vcp product_id]
	set index [dict get $vcp_dict $vcp composite_index]
	puts "  Vendor ID: 0x$vid"
	puts "  Product ID: 0x$pid"
	puts "  Composite index: $index"
	if {[lsearch -exact -nocase $vid_match_list $vid] >= 0} {
	    if {$index == 1} {
		# index = 1 is the ISP programmer, 3 is the UART
		puts "Found Pololu v2.1 ISP programmer at $vcp"
		return $vcp
	    }
	}
    }
    return ""
}

proc get_pololu_uart_vcp {} {
    # Return the Virtual Com Port corresponding to the Pololu USB/UART interface
    #
    # The Pololu vendor ID is 1FFB
    set vid_match_list [list 1FFB]
    set vcp_dict [detect_vcps]
    foreach {vcp} [dict keys $vcp_dict] {
	puts "Found $vcp"
	set vid [dict get $vcp_dict $vcp vendor_id]
	set pid [dict get $vcp_dict $vcp product_id]
	set index [dict get $vcp_dict $vcp composite_index]
	puts "  Vendor ID: 0x$vid"
	puts "  Product ID: 0x$pid"
	puts "  Composite index: $index"
	if {[lsearch -exact -nocase $vid_match_list $vid] >= 0} {
	    if {$index == 3} {
		# index = 1 is the ISP programmer, 3 is the UART
		puts "Found Pololu v2.1 UART interface at $vcp"
		return $vcp
	    }
	}
    }
    return ""
}

proc get_arduino_vcp {} {
    # Return the Virtual Com Port corresponding to an Arduino
    #
    # Arduinos with AVR-based USB/serial parts have multiple vendor IDs
    set vid_match_list [list 2A03 2341]
    # FTDI-based Arduinos have the FTDI vendor
    lappend vid_match_list 403
    set vcp_dict [detect_vcps]
    foreach {vcp} [dict keys $vcp_dict] {
	puts "Found $vcp"
	set vid [dict get $vcp_dict $vcp vendor_id]
	set pid [dict get $vcp_dict $vcp product_id]
	set index [dict get $vcp_dict $vcp composite_index]
	puts "  Vendor ID: 0x$vid"
	puts "  Product ID: 0x$pid"
	puts "  Composite index: $index"
	if {[lsearch -exact -nocase $vid_match_list $vid] >= 0} {
	    puts "Found Arduino at $vcp"
	    return $vcp
	}
    }
    return ""
}

proc get_espruino_original_vcp {} {
    # Return the Virtual Com Port corresponding to an Original
    # Espruino board
    #
    # Original Espruino boards have a STMicroelectronics (VID =
    # 0x0483) uC with a PID = 0x5740.
    set vid_match_list [list 483]
    set vcp_dict [detect_vcps]
    foreach {vcp} [dict keys $vcp_dict] {
	puts "Found $vcp"
	set vid [dict get $vcp_dict $vcp vendor_id]
	set pid [dict get $vcp_dict $vcp product_id]
	set index [dict get $vcp_dict $vcp composite_index]
	puts "  Vendor ID: 0x$vid"
	puts "  Product ID: 0x$pid"
	puts "  Composite index: $index"
	if {[lsearch -exact -nocase $vid_match_list $vid] >= 0} {
	    puts "Found Original Espruino at $vcp"
	    return $vcp
	}
    }
    return ""
}

proc get_qinheng_rs232_vcp {} {
    # Return the Virtual Com Port corresponding to the QinHeng
    # USB/RS-232 cable.
    #
    # QinHeng USB/RS-232 cables have VID = 0x1A86 and PID = 7523.
    set vid_match_list [list 1a86]
    set pid_match_list [list 7523]
    set vcp_dict [detect_vcps]
    foreach {vcp} [dict keys $vcp_dict] {
	puts "Found $vcp"
	set vid [dict get $vcp_dict $vcp vendor_id]
	set pid [dict get $vcp_dict $vcp product_id]
	set index [dict get $vcp_dict $vcp composite_index]
	puts "  Vendor ID: 0x$vid"
	puts "  Product ID: 0x$pid"
	puts "  Composite index: $index"
	if {[lsearch -exact -nocase $vid_match_list $vid] >= 0 &&
	    [lsearch -exact -nocase $pid_match_list $pid] >= 0} {
	    puts "Found QinHeng USB/RS-232 at $vcp"
	    return $vcp
	}
    }
    return ""
}

proc get_qinheng_rs485_vcp {} {
    # Return the Virtual Com Port corresponding to the QinHeng
    # USB/RS-232 cable.
    #
    # QinHeng USB/RS-232 cables have VID = 0x1A86 and PID = 7523.
    set vid_match_list [list 1a86]
    set pid_match_list [list 5523]
    set vcp_dict [detect_vcps]
    foreach {vcp} [dict keys $vcp_dict] {
	puts "Found $vcp"
	set vid [dict get $vcp_dict $vcp vendor_id]
	set pid [dict get $vcp_dict $vcp product_id]
	set index [dict get $vcp_dict $vcp composite_index]
	puts "  Vendor ID: 0x$vid"
	puts "  Product ID: 0x$pid"
	puts "  Composite index: $index"
	if {[lsearch -exact -nocase $vid_match_list $vid] >= 0 &&
	    [lsearch -exact -nocase $pid_match_list $pid] >= 0} {
	    puts "Found QinHeng USB/RS-232 at $vcp"
	    return $vcp
	}
    }
    return ""
}

proc cp2104_friend_vcp {} {
    # Return the Virtual Com Port corresponding to the Adafruit CP2104
    # Friend board
    #
    # CP2104 Friend boards have VID = 0x10C4 and PID = 0xEA60
    set vid_match_list [list 10c4]
    set pid_match_list [list ea60]
    set vcp_dict [detect_vcps]
    foreach {vcp} [dict keys $vcp_dict] {
	puts "Found $vcp"
	set vid [dict get $vcp_dict $vcp vendor_id]
	set pid [dict get $vcp_dict $vcp product_id]
	set index [dict get $vcp_dict $vcp composite_index]
	puts "  Vendor ID: 0x$vid"
	puts "  Product ID: 0x$pid"
	puts "  Composite index: $index"
	if {[lsearch -exact -nocase $vid_match_list $vid] >= 0 &&
	    [lsearch -exact -nocase $pid_match_list $pid] >= 0} {
	    puts "Found CP2104 Friend at $vcp"
	    return $vcp
	}
    }
    return ""
}

proc get_teensyduino_acm_vcp {} {
    # Return the Virtual Com Port corresponding to the Teensy 4.0's
    # Abstract Control Model (serial port) interface.
    #
    # Teensyduino Serial uses VID: 0x16c0 and PID: 0x483
    set vid_match_list [list 16c0]
    set pid_match_list [list 483]
    set vcp_dict [detect_vcps]
    foreach {vcp} [dict keys $vcp_dict] {
	puts "Found $vcp"
	set vid [dict get $vcp_dict $vcp vendor_id]
	set pid [dict get $vcp_dict $vcp product_id]
	set index [dict get $vcp_dict $vcp composite_index]
	puts "  Vendor ID: 0x$vid"
	puts "  Product ID: 0x$pid"
	puts "  Composite index: $index"
	if {[lsearch -exact -nocase $vid_match_list $vid] >= 0 &&
	    [lsearch -exact -nocase $pid_match_list $pid] >= 0} {
	    puts "Found Teensy serial interface at $vcp"
	    return $vcp
	}
    }
    return ""
}

proc grep {pattern args matches} {
    # Taken from DKF at https://wiki.tcl-lang.org/page/grep
    #
    # Restrict number of matches since I'm just using this to detect a
    # pattern in a file.
    #
    # Arguments:
    #   pattern -- The pattern to look for
    #   args -- Filename or stdin
    #   matches -- Maximum number of match lines to return
    if {[llength $args] == 0} {
        # read from stdin
        set lnum 0
        while {[gets stdin line] >= 0} {
            incr lnum
            if {[regexp $pattern $line]} {
                lappend match_line_list "${lnum}:${line}"
            }
        }
    } else {
        foreach filename $args {
            set file [open $filename r]
            set lnum 0
            while {[gets $file line] >= 0} {
                incr lnum
                if {[regexp $pattern $line]} {
                    lappend match_line_list "${filename}:${lnum}:${line}"
                }
            }
            close $file
        }
    }
    return $match_line_list
}

proc brltty_installed {} {
    # Check the system for brltty, which interferes with USB/serial
    # devices.  Returns true if brltty traces were found.
    switch $::tcl_platform(os) {
	{Linux} {
	    set line_match_list [grep brltty /var/log/syslog 1]
	    if {[llength $line_match_list] >= 1} {
		# There's something in syslog that shows brltty is present
		return true
	    } else {
		return false
	    }
	}
	{Windows NT} {
	    # Don't know if this is a problem on Windows
	    return false
	}
    }
}

proc brltty_check {} {
    if [brltty_installed] {
	puts ""
	puts "* Warning -- brltty detected in /var/log/syslog. This can interfere with serial devices"
	puts "* Uninstalling brltty will fix this"
	puts ""
    }
}

    

if {$params(d?)} {
    # List the known devices
    set name_width 20
    set description_width 30
    set format_string "%-*s %-*s"
    set header [format $format_string $name_width "Device" \
		    $description_width "Description"]
    puts $header
    puts [dashline [string length $header]]

    puts [format $format_string $name_width "pololu_isp" \
	      $description_width \
	      "Pololu USB AVR Programmer v2.1 ISP channel"]

    puts [format $format_string $name_width "pololu_uart" \
	      $description_width \
	      "Pololu USB AVR Programmer v2.1 USB/UART channel"]

    puts [format $format_string $name_width "arduino" \
	      $description_width \
	      "Arduino USB/UART interface"]
    puts [format $format_string $name_width "espruino_original" \
	      $description_width \
	      "Original Espruino board"]
    puts [format $format_string $name_width "qinheng_rs232" \
	      $description_width \
	      "QinHeng Electronics USB/RS-232 cable based on the CH340 IC"]
    puts [format $format_string $name_width "qinheng_rs485" \
	      $description_width \
	      "QinHeng Electronics USB/RS-485 adaptor based on the CH341 IC"]
    puts [format $format_string $name_width "teensyduino_acm" \
	      $description_width \
	      "Teensyduino Abstract Control Mode (serial) channel"]
    puts [format $format_string $name_width "cp2104_friend" \
	      $description_width \
	      "Adafruit CP2104 Friend board"]
    exit

}

set output_file_name $params(o)

# Open the output file
try {
    set fid [open $invoked_directory/$output_file_name w]
    puts "Opened $output_file_name"
} trap {} {message optdict} {
    puts $message
    exit
}

if {$params(a)} {
    # Print all discovered devices
    set vcp_dict [detect_vcps]
    brltty_check
    exit
}

set vcp ""
switch $params(d) {
    "pololu_isp" {
	set vcp [get_pololu_isp_vcp]
    }
    "pololu_uart" {
	set vcp [get_pololu_uart_vcp]
    }
    "arduino" {
	set vcp [get_arduino_vcp]
    }
    "espruino_original" {
	set vcp [get_espruino_original_vcp]
    }
    "qinheng_rs232" {
	set vcp [get_qinheng_rs232_vcp]
    }
    "qinheng_rs485" {
	set vcp [get_qinheng_rs485_vcp]
    }
    "teensyduino_acm" {
	set vcp [get_teensyduino_acm_vcp]
    }
    "cp2104_friend" {
	set vcp [cp2104_friend_vcp]
    }
    default {
	puts "Unrecognized -d parameter: $params(d)"
	exit
    }
}

if {$vcp eq ""} {
    puts "Did not find any virtual COM port for $params(d)"
    brltty_check
} else {
    puts $fid $vcp
}
close $fid
